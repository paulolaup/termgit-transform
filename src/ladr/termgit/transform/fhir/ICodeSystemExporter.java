package ladr.termgit.transform.fhir;

import java.util.Set;

public interface ICodeSystemExporter<T, U, V> {

    /**
     * Exports a ValueSet data instance associated with the CodeSystem data instance. Depending on the chosen
     * composition rule, the ValueSet instance may or may not contain the concepts directly or just references the
     * CodeSystem instance. Note that not all composition rule may be supported in which case a
     * CompositionRuleNotSupportedException will be thrown. Check available composition rules with
     * supportedValueSetCompositionRules and supportsValueSetCompositionRule methods.
     *
     * @param data Object containing the data exported to new format
     * @param csDest Target to export code system data to
     * @param vsDest Target to export value set data to
     * @param compositionRule Composition rule to apply during export of value set
     *
     * @throws Exception Thrown if export fails for any reason
     */
    void exportWithAssociatedValueSet(T data, U csDest, V vsDest, ValueSetCompositionRules compositionRule) throws Exception;

    /**
     * Returns set of supported composition rules for value set export
     *
     * @return Set instance containing ValueSetCompositionRules enum constants indicating supported composition rules.
     *         If none are supported an empty set shall be returned.
     */
    Set<ValueSetCompositionRules> supportedValueSetCompositionRules();

    /**
     * Checks if provided composition rule is supported by implementing exporter classes instance
     *
     * @param compositionRule ValueSetCompositionRules enum constant representing composition rule for which support
     *                        shall be checked
     * @return boolean value indicating the support status
     */
    boolean supportsValueSetCompositionRule(ValueSetCompositionRules compositionRule);

}
