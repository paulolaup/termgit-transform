package ladr.termgit.transform.fhir;

/**
 * IMPLICIT - Include all concepts indirectly by just mentioning the included code system (in case of FHIR instance data
 *            via only specifying one ValueSet.compose.include element and only assigning its system element with the
 *            URL of the code system)
 * EXPLICIT - Include all concepts directly
 * EXPANDED - Represent expanded form of value set by including all concepts directly into the ValueSet data instance
 *            (in case of FHIR instance data via the ValueSet.expansion element)
 */
public enum ValueSetCompositionRules {

    IMPLICIT, EXPLICIT, EXPANDED

}
