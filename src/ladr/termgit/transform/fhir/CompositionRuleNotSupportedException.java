package ladr.termgit.transform.fhir;

public class CompositionRuleNotSupportedException extends Exception {

    public CompositionRuleNotSupportedException(ValueSetCompositionRules compositionRule) {
        super("Code system exporter does not support composition rule "+compositionRule.toString()+"!");
    }

    public CompositionRuleNotSupportedException(ValueSetCompositionRules compositionRule, Throwable cause) {
        super("Code system exporter does not support composition rule "+compositionRule.toString()+"!", cause);
    }

}
