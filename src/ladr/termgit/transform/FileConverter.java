package ladr.termgit.transform;

import java.io.*;

public abstract class FileConverter<T> implements IFileConverter<T> {

    protected static BufferedReader readFromFile(File src) throws IOException {
        final var srcPath = src.getAbsolutePath();
        try {
            return new BufferedReader(new FileReader(srcPath));
        }
        catch (IOException ioException) {
            throw new IOException("Could not open source file @ "+srcPath, ioException);
        }
    }

}
