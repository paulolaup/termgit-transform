package ladr.termgit.transform;

public interface IBuilder<T> {

    /**
     * Terminates build process and returns configured instance of class. Internal state of the builder instance should
     * be reset by resetting fields to default values.
     *
     * @return instance of class T
     * @throws Exception if object creation fails
     */
    public T build() throws Exception;

}
