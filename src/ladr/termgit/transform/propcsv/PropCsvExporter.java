package ladr.termgit.transform.propcsv;

import ladr.termgit.transform.FileExportException;

import ladr.termgit.transform.FileExporter;
import ladr.termgit.transform.IFileExporter;
import ladr.termgit.transform.datamodel.cardinality.CardinalityException;
import ladr.termgit.transform.datamodel.cardinality.HasCardinality;
import ladr.termgit.transform.datamodel.cardinality.ICardinality;
import ladr.termgit.transform.datamodel.Table;
import ladr.termgit.transform.datamodel.Tuple;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Exports in the Proprietary CSV format as defined by TerminoloGit (.1.propcsv.csv) which is used as the main
 * file format to manage terminology resources.
 * 
 * Format:
 * 
 * column_1|column_2|...|column_n   # Metadata column names
 * --------------------------------
 * value_1 |value_2 |...|value_n    # Metadata column values
 * --------------------------------
 *                                  # Empty line for spacing: Presence is mandatory!
 * --------------------------------
 * column_1 |column_2 |...|column_m # Concept column names
 * --------------------------------
 * value_11 |value_12 |...|value_1m #
 * -------------------------------- #
 * ...                              # Rows containing concept entries
 * -------------------------------- #
 * value_k1 |value_k2 |...|value_km #
 * 
 * 
 * @author paulolaup
 *
 */
public abstract class PropCsvExporter
        <METADATACOLUMNS extends Enum<METADATACOLUMNS> & HasCardinality<C1>,
        DATACOLUMNS extends Enum<DATACOLUMNS> & HasCardinality<C2>,
        C1 extends ICardinality, C2 extends ICardinality>
        extends FileExporter implements IFileExporter<Table> {

    public static final char DEFAULT_DEST_SEPERATOR = ';';
    public static final char DEFAULT_DEST_VALUE_WRAPPER = '"';

    private final char destSeperator;
    private final char destValueWrapper;
    private final HashMap<String[], Predicate<String[]>> filters;
    // TODO: Switch to arrays instead of lists in the future since size should be know through use of builder object
    private final HashMap<DATACOLUMNS, List<Supplier<String>>> staticTransformations;
    private final HashMap<DATACOLUMNS, List<Tuple<String[], Function<String[], String>>>> columnTransformations;
    private final HashMap<METADATACOLUMNS, List<String>> metadataValues;

    public PropCsvExporter(char destSeperator, char destValueWrapper,
    					   HashMap<String[], Predicate<String[]>> filters,
    					   HashMap<DATACOLUMNS, List<Supplier<String>>> staticTransformations,
    					   HashMap<DATACOLUMNS, List<Tuple<String[], Function<String[], String>>>> columnTransformations,
                           HashMap<METADATACOLUMNS, List<String>> metadataValues) throws Exception {
        this.destSeperator = destSeperator;
        this.destValueWrapper = destValueWrapper;
        this.filters = filters;
        this.staticTransformations = staticTransformations;
        this.columnTransformations = columnTransformations;
        this.metadataValues = metadataValues;
        checkCardinalities();
    }

    @Override
    public abstract void export(Table data, File dest) throws IOException, FileExportException;
    
    @Override
    public String getExtension() {
        return ".1.propcsv.csv";
    }
    
    protected HashMap<DATACOLUMNS, List<Tuple<Integer[], Function<String[], String>>>> getColumnTransformationIndex(String[] tableColumnNames) throws Exception {
    	final var columnNamesIdx = getColumnNamesIdx(tableColumnNames);
    	
    	final var columnTransformationIdx = new HashMap<DATACOLUMNS, List<Tuple<Integer[], Function<String[], String>>>>();
    	for(var entry: this.columnTransformations.entrySet()) {
            final var destColumn = entry.getKey();
            final var transformationTuples = new ArrayList<Tuple<Integer[], Function<String[], String>>>(entry.getValue().size());
            for(var transformationTuple: entry.getValue()) {
                final var indices = (Integer[]) Arrays.stream(transformationTuple.first()).mapToInt(columnNamesIdx::get).boxed().toArray(Integer[]::new);
                transformationTuples.add(new Tuple<>(indices, transformationTuple.second()));
            }
            columnTransformationIdx.put(destColumn, transformationTuples);
    	}
    	return columnTransformationIdx;
    }
    
    protected Map<String, Integer> getColumnNamesIdx(final String[] columnNames) throws Exception {
    	checkForDuplicates(columnNames);
    	final var columnNamesIdx = new HashMap<String, Integer>();
    	for(int i = 0; i < columnNames.length; i++) {
    		columnNamesIdx.put(columnNames[i], i);
    	}
    	return columnNamesIdx;
    }
    
    private static void checkForDuplicates(final String[] strings) throws Exception {
    	try {
    		Set.of(strings);
    	}
    	catch (IllegalArgumentException exception) {
    		throw new Exception("Array contained duplicates", exception);
    	}
    }

    private void checkCardinalities() throws Exception {
        try {
            final var metadataColumnEnumClass = getMetadataColumnEnumClass();
            checkRequired(metadataColumnEnumClass, this.metadataValues.keySet());
            checkMapCardinality(this.metadataValues);
        }
        catch (CardinalityException exception) {
            throw new Exception("Metadata column failed cardinality check", exception);
        }
        try {
            final var conceptColumnEnumClass = getDataColumnEnumClass();
            final var coveredColumns = new HashSet<>(this.staticTransformations.keySet());
            coveredColumns.addAll(this.columnTransformations.keySet());
            checkRequired(conceptColumnEnumClass, coveredColumns);
            checkMapCardinality(this.staticTransformations);
            checkMapCardinality(this.columnTransformations);
        }
        catch (CardinalityException exception) {
            throw new Exception("Concept column failed cardinality check", exception);
        }
    }

    private static void checkMapCardinality(Map<? extends HasCardinality<?>, ? extends List<?>> map) throws CardinalityException {
        for (var entry: map.entrySet()) {
            final var column = entry.getKey();
            final var cardinality = column.getCardinality();
            final var number = entry.getValue().size();
            if (!cardinality.inBounds(number)) {
                final var bounds = cardinality.bounds();
                throw new CardinalityException("Cardinality violation for column " + column +
                        ": " + number + " not in range [" + bounds[0] + ":" + bounds[1] + "]");
            }
        }
    }

    private <E extends Enum<E> & HasCardinality<?>> void checkRequired(Class<E> clazz, Set<E> set) throws CardinalityException {
        for (var column: clazz.getEnumConstants()) {
            if(!set.contains(column) && column.isRequired()) {
                final var bounds = column.getCardinality().bounds();
                throw new CardinalityException("Cardinality violation for column " + column +
                        ": 0 not in range [" + bounds[0] + ":" + bounds[1] + "]");
            }
        }
    }
    
    protected boolean checkFilters(String[] columnValues, Map<String, Integer> columnIdx) {
    	return this.filters.entrySet().stream().allMatch(filterEntry -> {
    		final var srcColumnValues = (String[]) Arrays.stream(filterEntry.getKey()).map(c -> columnValues[columnIdx.get(c)]).toArray(String[]::new);
        	final var filter = filterEntry.getValue();
        	return filter.test(srcColumnValues);
    	});
    }

    protected abstract Class<METADATACOLUMNS> getMetadataColumnEnumClass();

    protected abstract Class<DATACOLUMNS> getDataColumnEnumClass();

    public char getDestSeperator() {
        return destSeperator;
    }

    public char getDestValueWrapper() {
        return destValueWrapper;
    }
    
    public HashMap<String[], Predicate<String[]>> getFilters() {
    	return this.filters;
    }

    public HashMap<DATACOLUMNS, List<Supplier<String>>> getStaticTransformations() {
		return staticTransformations;
	}

	public HashMap<DATACOLUMNS, List<Tuple<String[], Function<String[], String>>>> getColumnTransformations() {
		return columnTransformations;
	}

	public HashMap<METADATACOLUMNS, List<String>> getMetadataValues() {
        return metadataValues;
    }

    public enum ColumnCardinality implements ICardinality {

        AT_MOST_ONCE(0, 1), EXACTLY_ONCE(1, 1), AT_LEAST_ONCE(1, Integer.MAX_VALUE),
        ANY(0, Integer.MAX_VALUE);

        private final int lower;
        private final int upper;

        ColumnCardinality(int lower, int upper) {
            this.lower = lower;
            this.upper = upper;
        }

        public int lower() { return this.lower; }

        public int upper() { return this.upper; }

        public int[] bounds() { return new int[]{this.lower, this.upper}; }

        public boolean inBounds(int number) {
            return (number >= this.lower && number <= this.upper);
        }

        public boolean isRequired() {
            return this.lower > 0;
        }

    }

}
