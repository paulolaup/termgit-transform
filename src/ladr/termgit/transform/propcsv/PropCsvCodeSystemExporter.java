package ladr.termgit.transform.propcsv;

import ladr.termgit.transform.FileExportException;
import ladr.termgit.transform.FileExporter;
import ladr.termgit.transform.IBuilder;
import ladr.termgit.transform.datamodel.cardinality.HasCardinality;
import ladr.termgit.transform.datamodel.Table;
import ladr.termgit.transform.datamodel.Tuple;
import ladr.termgit.transform.fhir.CompositionRuleNotSupportedException;
import ladr.termgit.transform.fhir.ICodeSystemExporter;
import ladr.termgit.transform.fhir.ValueSetCompositionRules;
import ladr.termgit.transform.propcsv.PropCsvExporter.ColumnCardinality;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

// TODO: Add capabilities to handle designation and property for concept data
// TODO: Add capabilities for handling multiple identifier entries
public class PropCsvCodeSystemExporter
        extends PropCsvExporter
                <PropCsvCodeSystemExporter.MetadataColumns, PropCsvCodeSystemExporter.ConceptColumns,
                PropCsvExporter.ColumnCardinality, PropCsvExporter.ColumnCardinality>
        implements ICodeSystemExporter<Table, File, File> {

    private static final Set<ValueSetCompositionRules> supportedValueSetCompositionRules = Set.of(ValueSetCompositionRules.EXPLICIT);

    public PropCsvCodeSystemExporter(char destSeperator, char destValueWrapper,
    					   HashMap<String[], Predicate<String[]>> filters,
                           HashMap<PropCsvCodeSystemExporter.ConceptColumns, List<Supplier<String>>> staticTransformations,
                           HashMap<PropCsvCodeSystemExporter.ConceptColumns, List<Tuple<String[], Function<String[], String>>>> columnTransformations,
                           HashMap<PropCsvCodeSystemExporter.MetadataColumns, List<String>> metadataValues) throws Exception {
        super(destSeperator, destValueWrapper, filters, staticTransformations, columnTransformations, metadataValues);
    }
    

    @Override
    public void export(Table data, File dest) throws IOException, FileExportException {
        System.out.println("Exporting code system to file @ " + dest.getAbsolutePath());

        try (var writer = FileExporter.writeToFile(dest)) {
            final var seperatorString = String.valueOf(super.getDestSeperator());
            final var valueWrapper = super.getDestValueWrapper();

            // Write metadata to file
            final var columnLineBuilder = new StringJoiner(seperatorString);
            final var valueLineBuilder = new StringJoiner(seperatorString);
            for (var metadataColumn : MetadataColumns.values()){
                final var values = super.getMetadataValues().get(metadataColumn);
                if (values == null || values.size() == 0) {
                    columnLineBuilder.add(valueWrapper + metadataColumn.toString() + valueWrapper);
                    valueLineBuilder.add(valueWrapper + "" + valueWrapper);
                    continue;
                }
                values.forEach(value -> {
                    if (value == null) value = "";
                    columnLineBuilder.add(valueWrapper + metadataColumn.toString() + valueWrapper);
                    valueLineBuilder.add(valueWrapper + value + valueWrapper);
                });
            }
            writeLine(columnLineBuilder.toString(), writer);
            writeLine(valueLineBuilder.toString(), writer);
            // Write empty line as spacer. Important: TerminoloGit expects this!
            writeLine("", writer);

            // Write concept columns to file
            final var staticTransformations = super.getStaticTransformations();
            final var columnTransformations = super.getColumnTransformations();
            final var conceptsColumns = ConceptColumns.values();
            final var conceptColumnBuilder = new StringJoiner(seperatorString);
            for (var conceptColumn : conceptsColumns) {
                // Add column name at least once or as often as it appears otherwise
                final var staticCnt = staticTransformations.containsKey(conceptColumn) ? staticTransformations.get(conceptColumn).size() : 0;
                final var dynamicCnt = columnTransformations.containsKey(conceptColumn) ? columnTransformations.get(conceptColumn).size() : 0;
                var cnt = staticCnt + dynamicCnt;
                final var headerSting = valueWrapper + conceptColumn.toString() + valueWrapper;
                // Do-while ensured that at least column name is written at least once
                do {
                    conceptColumnBuilder.add(headerSting);
                    cnt--;
                } while (cnt > 0);
            }
            writeLine(conceptColumnBuilder.toString(), writer);
            
            // Write concept data to file
            try {
            	final var columnTransformationsIndexed = super.getColumnTransformationIndex(data.getColumnNames());
            	final var columnNamesIdx = super.getColumnNamesIdx(data.getColumnNames());
            	var lineCnt = 0;
                for (var entry : data) {
                    System.out.print("Line " + lineCnt + "\r");
                    final var lineBuilder = new StringJoiner(seperatorString);
                    final var passedFilters = super.checkFilters(entry, columnNamesIdx);
                    
                    if (passedFilters) {
                    	for (var conceptColumn : conceptsColumns) {
                            if (columnTransformationsIndexed.containsKey(conceptColumn)) {
                            	final var transformationTuples = columnTransformationsIndexed.get(conceptColumn);
                                transformationTuples.forEach(transformationTuple -> {
                                    final var values = (String[]) Arrays.stream(transformationTuple.first()).map(i -> entry[i]).toArray(String[]::new);
                                    final var transformation = transformationTuple.second();
                                    lineBuilder.add(valueWrapper + transformation.apply(values) + valueWrapper);
                                });
                            }
                            else if (staticTransformations.containsKey(conceptColumn)) {
                                final var transformations = staticTransformations.get(conceptColumn);
                                transformations.forEach(transformation -> lineBuilder.add(valueWrapper + transformation.get() + valueWrapper));
                            }
                            else {
                                lineBuilder.add(valueWrapper+""+valueWrapper);
                            }
                        }

                        writeLine(lineBuilder.toString(), writer);
                    }
                    
                    lineCnt++;
                }
            }
            catch (Exception exception) {
            	throw new FileExportException("Failed to write code system concept data", exception);
            }
            finally {
            	System.out.println("Saved results to file");
            }

        }

    }

    @Override
    public void exportWithAssociatedValueSet(Table data, File csDest, File vsDest, ValueSetCompositionRules compositionRule) throws Exception {
        // Export code system to file
        this.export(data, csDest);

        if (!this.supportsValueSetCompositionRule(compositionRule)) throw new CompositionRuleNotSupportedException(compositionRule);

        final var builder = PropCsvValueSetExporter.build()
                .destSeperator(this.getDestSeperator())
                .destValueWrapper(this.getDestValueWrapper());

        // Add matching metadata values
        final var metadataValues = this.getMetadataValues();
        for (var ccMetadataColumn: PropCsvCodeSystemExporter.MetadataColumns.values()) {
        	// Skip resource type metadata since it is fixed for each propcsv exporter depending on the represented FHIR resource type
        	if (ccMetadataColumn == PropCsvCodeSystemExporter.MetadataColumns.RESOURCE) continue;
            // Get corresponding metadata column for value sets if it exists
            try {
                final var vsMetadataColumn = PropCsvValueSetExporter.MetadataColumns.valueOf(ccMetadataColumn.name());
                final var metadataColumnValues = metadataValues.get(ccMetadataColumn);
                builder.setMetadata(vsMetadataColumn, metadataColumnValues == null? null : metadataColumnValues.get(0));
            }
            catch (IllegalArgumentException exception) {
                // Ignore exception since it only means that no matching column was found
            }
        }
        
        // Add filters
        try {
        	for (var entry: super.getFilters().entrySet()) {
        		builder.addFilter(entry.getValue(), entry.getKey());
        	}
        }
        catch (Exception exception) {
        	throw new Exception("Failed to add filters", exception);
        }

        // Add transformations
        final var system = metadataValues.get(PropCsvCodeSystemExporter.MetadataColumns.URL).get(0);
        builder.forCodeSystem(system);
        
        try {
        	this.mapTransformation(PropCsvCodeSystemExporter.ConceptColumns.CODE, PropCsvValueSetExporter.ConceptColumns.CODE, builder);
        }
        catch (Exception exception) {
            throw new Exception("Failed to add transformation for code column", exception);
        }
        try {
        	this.mapTransformation(PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, PropCsvValueSetExporter.ConceptColumns.DISPLAY, builder);
        }
        catch (Exception exception) {
        	throw new Exception("Failed to add transformation for display column", exception);
        }

        // Export value set data to file
        final var valueSetExporter = builder.build();
        valueSetExporter.export(data, vsDest);
    }
    
    private void mapTransformation(PropCsvCodeSystemExporter.ConceptColumns csColumn, PropCsvValueSetExporter.ConceptColumns vsColumn, PropCsvValueSetExporter.Builder builder) throws Exception {
    	final var columnTransformations = super.getColumnTransformations();
    	final var staticTransformations = super.getStaticTransformations();
    	if(columnTransformations.containsKey(csColumn)) {
            for(var transformationTuple: columnTransformations.get(csColumn)) {
                builder.addColumnTransformation(transformationTuple.second(), vsColumn, transformationTuple.first());
            }
    	}
    	else if (staticTransformations.containsKey(csColumn)) {
            for(var supplier: staticTransformations.get(csColumn)) {
                builder.addStaticTransformation(supplier, vsColumn);
            }
    	}
    	else if (vsColumn.isRequired()) {
    		throw new Exception("No transformation specified for code system concept column "+csColumn.toString());
    	}
    }

    @Override
    protected Class<MetadataColumns> getMetadataColumnEnumClass() {
        return PropCsvCodeSystemExporter.MetadataColumns.class;
    }

    @Override
    protected Class<ConceptColumns> getDataColumnEnumClass() {
        return PropCsvCodeSystemExporter.ConceptColumns.class;
    }

    @Override
    public Set<ValueSetCompositionRules> supportedValueSetCompositionRules() {
        return supportedValueSetCompositionRules;
    }

    @Override
    public boolean supportsValueSetCompositionRule(ValueSetCompositionRules compositionRule) {
        return supportedValueSetCompositionRules.contains(compositionRule);
    }

    public static PropCsvCodeSystemExporter.Builder builder() {
        return new PropCsvCodeSystemExporter.Builder();
    }

    public static class Builder implements IBuilder<PropCsvCodeSystemExporter> {

        private char destSeperator =  PropCsvExporter.DEFAULT_DEST_SEPERATOR;
        private char destValueWrapper = PropCsvExporter.DEFAULT_DEST_VALUE_WRAPPER;
        private HashMap<String[], Predicate<String[]>> filters = new HashMap<>();
        private HashMap<ConceptColumns, List<Supplier<String>>> staticTransformations = new HashMap<>();
        private HashMap<ConceptColumns, List<Tuple<String[], Function<String[], String>>>> columnTransformations = new HashMap<>();
        // private HashMap<ConceptColumns, String> columnMapping = new HashMap<>();
        private HashMap<MetadataColumns, List<String>> metadataValues = new HashMap<>();
        
        private Builder() {
        	this.metadataValues.put(PropCsvCodeSystemExporter.MetadataColumns.RESOURCE, List.of("CodeSystem"));
        }

        public PropCsvCodeSystemExporter.Builder destSeperator(char seperator) {
            this.destSeperator = seperator;
            return this;
        }

        public PropCsvCodeSystemExporter.Builder destValueWrapper(char valueWrapper) {
            this.destValueWrapper = valueWrapper;
            return this;
        }
        
        public PropCsvCodeSystemExporter.Builder addFilter(Predicate<String[]> filter, String... srcColumns) throws Exception {
        	if (this.filters.containsKey(srcColumns))
        		throw new Exception("Filter for source columns '"+srcColumns.toString()+"' already defined");
        	this.filters.put(srcColumns, filter);
        	return this;
        }

        public PropCsvCodeSystemExporter.Builder addStaticTransformation(Supplier<String> transformation, ConceptColumns destColumn) {
            // if (this.columnTransformations.containsKey(destColumn) || this.staticTransformations.containsKey(destColumn))
            //     throw new Exception("Transformation for destination column + '" + destColumn.toString() + "' already present");
            if(!this.staticTransformations.containsKey(destColumn)) this.staticTransformations.put(destColumn, new ArrayList<>());
            this.staticTransformations.get(destColumn).add(transformation);
            return this;
        }

        public PropCsvCodeSystemExporter.Builder addColumnTransformation(Function<String[], String> transformation, ConceptColumns destColumn, String... srcColumns) {
            // if (this.columnTransformations.containsKey(destColumn) || this.staticTransformations.containsKey(destColumn))
            //    throw new Exception("Transformation for destination column + '" + destColumn.toString() + "' already present");
            if(!this.columnTransformations.containsKey(destColumn)) this.columnTransformations.put(destColumn, new ArrayList<>());
            this.columnTransformations.get(destColumn).add(new Tuple<String[], Function<String[], String>>(srcColumns, transformation));
            return this;
        }

        protected PropCsvCodeSystemExporter.Builder setMetadata(MetadataColumns column, String value) {
            final var list = value == null? new ArrayList<String>(): List.of(value);
            this.metadataValues.put(column, list);
            return this;
        }

        protected PropCsvCodeSystemExporter.Builder addMetadata(MetadataColumns column, String value) {
            if (!this.metadataValues.containsKey(column)) this.metadataValues.put(column, new ArrayList<>());
            this.metadataValues.get(column).add(value);
            return this;
        }

        public PropCsvCodeSystemExporter.Builder setId(String value) {
            return this.setMetadata(MetadataColumns.ID, value);
        }

        public PropCsvCodeSystemExporter.Builder setUrl(String value) {
            return this.setMetadata(MetadataColumns.URL, value);
        }

        public PropCsvCodeSystemExporter.Builder addIdentifier(String use, String type, String system, String value,
                                                               String period, String assigner) {
            final var joiner = new StringJoiner("|");
            joiner.add(use).add(type).add(system).add(value).add(period).add(assigner);
            return this.addMetadata(MetadataColumns.IDENTIFIER, joiner.toString());
        }

        public PropCsvCodeSystemExporter.Builder setDate(String value) {
            return this.setMetadata(MetadataColumns.DATE, value);
        }

        public PropCsvCodeSystemExporter.Builder setVersion(String value) {
            return this.setMetadata(MetadataColumns.VERSION, value);
        }

        public PropCsvCodeSystemExporter.Builder setName(String value) {
            return this.setMetadata(MetadataColumns.NAME, value);
        }

        public PropCsvCodeSystemExporter.Builder setTitle(String value) {
            return this.setMetadata(MetadataColumns.TITLE, value);
        }

        public PropCsvCodeSystemExporter.Builder setStatus(String value) {
            return this.setMetadata(MetadataColumns.STATUS, value);
        }

        public PropCsvCodeSystemExporter.Builder setDescription(String value) {
            return this.setMetadata(MetadataColumns.DESCRIPTION, value);
        }

        public PropCsvCodeSystemExporter.Builder setContent(String value) {
            return this.setMetadata(MetadataColumns.CONTENT, value);
        }

        public PropCsvCodeSystemExporter.Builder setCopyright(String value) {
            return this.setMetadata(MetadataColumns.COPYRIGHT, value);
        }

        public PropCsvCodeSystemExporter build() throws Exception {
            // Safe values in temp variables
            var destSeperator = this.destSeperator;
            var destValueWrapper = this.destValueWrapper;
            var filters = this.filters;
            var staticTransformations = this.staticTransformations;
            var columnTransformations = this.columnTransformations;
            var metadataValues = this.metadataValues;
            // Clear previous values and reset to default
            this.reset();
            return new PropCsvCodeSystemExporter(destSeperator, destValueWrapper, filters, staticTransformations,
                    columnTransformations, metadataValues);
        }

        public void reset() {
            this.destSeperator = PropCsvExporter.DEFAULT_DEST_SEPERATOR;
            this.destValueWrapper = PropCsvExporter.DEFAULT_DEST_VALUE_WRAPPER;
            this.filters = new HashMap<>();
            this.staticTransformations = new HashMap<>();
            this.columnTransformations = new HashMap<>();
            this.metadataValues = new HashMap<>();
        }

    }

    public enum MetadataColumns implements HasCardinality<ColumnCardinality> {

        RESOURCE("resource", ColumnCardinality.EXACTLY_ONCE), ID("id", ColumnCardinality.EXACTLY_ONCE),
        URL("url", ColumnCardinality.AT_MOST_ONCE), IDENTIFIER("identifier", ColumnCardinality.ANY),
        DATE("date", ColumnCardinality.AT_MOST_ONCE), VERSION("version", ColumnCardinality.AT_MOST_ONCE),
        NAME("name", ColumnCardinality.AT_MOST_ONCE), TITLE("title", ColumnCardinality.AT_MOST_ONCE),
        STATUS("status", ColumnCardinality.EXACTLY_ONCE), DESCRIPTION("description", ColumnCardinality.AT_MOST_ONCE),
        CONTENT("content", ColumnCardinality.AT_MOST_ONCE), COPYRIGHT("copyright", ColumnCardinality.AT_MOST_ONCE);

        private final String rowName;
        private final ColumnCardinality cardinality;

        MetadataColumns(String rowName) {
            this.rowName = rowName;
            this.cardinality = ColumnCardinality.AT_MOST_ONCE;
        }

        MetadataColumns(String rowName, ColumnCardinality cardinality) {
            this.rowName = rowName;
            this.cardinality = cardinality;
        }

        @Override
        public String toString() {
            return this.rowName;
        }

        public boolean isRequired() {
            return this.cardinality.isRequired();
        }

        @Override
        public ColumnCardinality getCardinality() {
            return this.cardinality;
        }

        @Override
        public boolean inBounds(int number) {
            return this.cardinality.inBounds(number);
        }

    }

    public enum ConceptColumns implements HasCardinality<ColumnCardinality> {

        CODE("code", ColumnCardinality.EXACTLY_ONCE),
        DISPLAY("display", ColumnCardinality.AT_MOST_ONCE),
        DESIGNATION("designation", ColumnCardinality.ANY),
        DEFINITION("definition", ColumnCardinality.AT_MOST_ONCE),
        PROPERTY("property", ColumnCardinality.ANY);

        private final String rowName;
        private final ColumnCardinality cardinality;

        ConceptColumns(String rowName) {
            this.rowName = rowName;
            this.cardinality = ColumnCardinality.AT_MOST_ONCE;
        }

        ConceptColumns(String rowName, ColumnCardinality cardinality) {
            this.rowName = rowName;
            this.cardinality = cardinality;
        }

        @Override
        public String toString() {
            return this.rowName;
        }

        public boolean isRequired() {
            return this.cardinality.isRequired();
        }

        @Override
        public ColumnCardinality getCardinality() {
            return this.cardinality;
        }

        @Override
        public boolean inBounds(int number) {
            return this.cardinality.inBounds(number);
        }

    }

}
