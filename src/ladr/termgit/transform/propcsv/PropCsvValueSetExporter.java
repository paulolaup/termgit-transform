package ladr.termgit.transform.propcsv;

import ladr.termgit.transform.FileExportException;
import ladr.termgit.transform.FileExporter;
import ladr.termgit.transform.IBuilder;
import ladr.termgit.transform.datamodel.Table;
import ladr.termgit.transform.datamodel.Tuple;
import ladr.termgit.transform.datamodel.cardinality.HasCardinality;
import ladr.termgit.transform.propcsv.PropCsvExporter.ColumnCardinality;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class PropCsvValueSetExporter
        extends PropCsvExporter
                <PropCsvValueSetExporter.MetadataColumns, PropCsvValueSetExporter.ConceptColumns,
                PropCsvExporter.ColumnCardinality, PropCsvExporter.ColumnCardinality> {

    public PropCsvValueSetExporter(char destSeperator, char destValueWrapper,
    								 HashMap<String[], Predicate<String[]>> filters,
                                     HashMap<PropCsvValueSetExporter.ConceptColumns, List<Supplier<String>>> staticTransformations,
                                     HashMap<PropCsvValueSetExporter.ConceptColumns, List<Tuple<String[], Function<String[], String>>>> columnTransformations,
                                     HashMap<PropCsvValueSetExporter.MetadataColumns, List<String>> metadataValues) throws Exception {
        super(destSeperator, destValueWrapper, filters, staticTransformations, columnTransformations, metadataValues);
    }

    @Override
    public void export(Table data, File dest) throws IOException, FileExportException {
        System.out.println("Exporting value set to file @ " + dest.getAbsolutePath());

        try (var writer = FileExporter.writeToFile(dest)) {
            final var seperatorString = String.valueOf(super.getDestSeperator());
            final var valueWrapper = super.getDestValueWrapper();

            // Write metadata to file
            final var columnLineBuilder = new StringJoiner(seperatorString);
            final var valueLineBuilder = new StringJoiner(seperatorString);
            for (var metadataColumn : PropCsvValueSetExporter.MetadataColumns.values()){
                var values = super.getMetadataValues().get(metadataColumn);
                if (values == null || values.size() == 0) {
                    columnLineBuilder.add(valueWrapper + metadataColumn.toString() + valueWrapper);
                    valueLineBuilder.add(valueWrapper + "" + valueWrapper);
                    continue;
                }
                values.forEach(value -> {
                    if (value == null) value = "";
                    columnLineBuilder.add(valueWrapper + metadataColumn.toString() + valueWrapper);
                    valueLineBuilder.add(valueWrapper + value + valueWrapper);
                });
            }
            writeLine(columnLineBuilder.toString(), writer);
            writeLine(valueLineBuilder.toString(), writer);
            // Write empty line as spacer. Important: TerminoloGit expects this!
            writeLine("", writer);

            // Write concept columns to file
            final var staticTransformations = super.getStaticTransformations();
            final var columnTransformations = super.getColumnTransformations();
            final var conceptsColumns = PropCsvValueSetExporter.ConceptColumns.values();
            final var conceptColumnBuilder = new StringJoiner(seperatorString);
            for (var conceptColumn : conceptsColumns) {
                // Add column name at least once or as often as it appears otherwise
                final var staticCnt = staticTransformations.containsKey(conceptColumn) ? staticTransformations.get(conceptColumn).size() : 0;
                final var dynamicCnt = columnTransformations.containsKey(conceptColumn) ? columnTransformations.get(conceptColumn).size() : 0;
                var cnt = staticCnt + dynamicCnt;
                final var headerSting = valueWrapper + conceptColumn.toString() + valueWrapper;
                // Do-while ensured that at least column name is written at least once
                do {
                    conceptColumnBuilder.add(headerSting);
                    cnt--;
                } while (cnt > 0);
            }
            writeLine(conceptColumnBuilder.toString(), writer);

            // Write concept data to file
            try {
            	final var columnTransformationsIndexed = super.getColumnTransformationIndex(data.getColumnNames());
                final var columnNamesIdx = super.getColumnNamesIdx(data.getColumnNames());
            	var lineCnt = 0;
                for (var entry : data) {
                    System.out.print("Line " + lineCnt + "\r");
                    final var lineBuilder = new StringJoiner(seperatorString);
                    final var passedFilters = super.checkFilters(entry, columnNamesIdx);
                    
                    if (passedFilters) {
                    	for (var conceptColumn : conceptsColumns) {
                            if (columnTransformationsIndexed.containsKey(conceptColumn)) {
                            	final var transformationTuples = columnTransformationsIndexed.get(conceptColumn);
                                transformationTuples.forEach(transformationTuple -> {
                                    final var values = (String[]) Arrays.stream(transformationTuple.first()).map(i -> entry[i]).toArray(String[]::new);
                                    final var transformation = transformationTuple.second();
                                    lineBuilder.add(valueWrapper + transformation.apply(values) + valueWrapper);
                                });
                            }
                            else if (staticTransformations.containsKey(conceptColumn)) {
                                final var transformations = staticTransformations.get(conceptColumn);
                                transformations.forEach(transformation -> lineBuilder.add(valueWrapper + transformation.get() + valueWrapper));
                            }
                            else {
                                lineBuilder.add(valueWrapper + "" + valueWrapper);
                            }
                        }

                        writeLine(lineBuilder.toString(), writer);
                    }
                    
                    lineCnt++;
                }
            }
            catch (Exception exception) {
            	throw new FileExportException("Failed to write value set concept data", exception);
            }
            finally {
            	System.out.println("Saved results to file");
            }
                

        }
    }

    @Override
    protected Class<PropCsvValueSetExporter.MetadataColumns> getMetadataColumnEnumClass() {
        return PropCsvValueSetExporter.MetadataColumns.class;
    }

    @Override
    protected Class<PropCsvValueSetExporter.ConceptColumns> getDataColumnEnumClass() {
        return PropCsvValueSetExporter.ConceptColumns.class;
    }

    public static PropCsvValueSetExporter.Builder build() {
        return new PropCsvValueSetExporter.Builder();
    }

    public static class Builder implements IBuilder<PropCsvValueSetExporter> {

        private char destSeperator =  PropCsvExporter.DEFAULT_DEST_SEPERATOR;
        private char destValueWrapper = PropCsvExporter.DEFAULT_DEST_VALUE_WRAPPER;
        private HashMap<String[], Predicate<String[]>> filters = new HashMap<>();
        private HashMap<PropCsvValueSetExporter.ConceptColumns, List<Supplier<String>>> staticTransformations = new HashMap<>();
        private HashMap<PropCsvValueSetExporter.ConceptColumns, List<Tuple<String[], Function<String[], String>>>> columnTransformations = new HashMap<>();
        private HashMap<PropCsvValueSetExporter.MetadataColumns, List<String>> metadataValues = new HashMap<>();
        
        private Builder() {
        	this.metadataValues.put(PropCsvValueSetExporter.MetadataColumns.RESOURCE, List.of("ValueSet"));
        }

        public PropCsvValueSetExporter.Builder forCodeSystem(String codeSystemUrl) {
            this.staticTransformations.put(ConceptColumns.SYSTEM, List.of(() -> codeSystemUrl));
            return this;
        }

        public PropCsvValueSetExporter.Builder destSeperator(char seperator) {
            this.destSeperator = seperator;
            return this;
        }

        public PropCsvValueSetExporter.Builder destValueWrapper(char valueWrapper) {
            this.destValueWrapper = valueWrapper;
            return this;
        }
        
        public PropCsvValueSetExporter.Builder addFilter(Predicate<String[]> filter, String... srcColumns) throws Exception {
        	if (this.filters.containsKey(srcColumns))
        		throw new Exception("Filter for source columns '"+srcColumns.toString()+"' already defined");
        	this.filters.put(srcColumns, filter);
        	return this;
        }

        public PropCsvValueSetExporter.Builder addStaticTransformation(Supplier<String> transformation, PropCsvValueSetExporter.ConceptColumns destColumn) throws Exception {
            // if (this.columnTransformations.containsKey(destColumn) || this.staticTransformations.containsKey(destColumn))
            //    throw new Exception("Transformation for destination column + '" + destColumn.toString() + "' already present");
            if(!this.staticTransformations.containsKey(destColumn)) this.staticTransformations.put(destColumn, new ArrayList<>());
            this.staticTransformations.get(destColumn).add(transformation);
            return this;
        }

        public PropCsvValueSetExporter.Builder addColumnTransformation(Function<String[], String> transformation, PropCsvValueSetExporter.ConceptColumns destColumn, String... srcColumns) throws Exception {
            // if (this.columnTransformations.containsKey(destColumn))
            //    throw new Exception("Transformation for destination column + '" + destColumn.toString() + "' already present");
            if(!this.columnTransformations.containsKey(destColumn)) this.columnTransformations.put(destColumn, new ArrayList<>());
            this.columnTransformations.get(destColumn).add(new Tuple<>(srcColumns, transformation));
            return this;
        }

        protected PropCsvValueSetExporter.Builder setMetadata(PropCsvValueSetExporter.MetadataColumns column, String value) {
            final var list = value == null? new ArrayList<String>(): List.of(value);
            this.metadataValues.put(column, list);
            return this;
        }

        protected PropCsvValueSetExporter.Builder addMetadata(PropCsvValueSetExporter.MetadataColumns column, String value) {
            if (!this.metadataValues.containsKey(column)) this.metadataValues.put(column, new ArrayList<>());
            this.metadataValues.get(column).add(value);
            return this;
        }

        public PropCsvValueSetExporter.Builder setId(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.ID, value);
        }

        public PropCsvValueSetExporter.Builder setUrl(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.URL, value);
        }

        public PropCsvValueSetExporter.Builder addIdentifier(String use, String type, String system, String value,
                                                               String period, String assigner) {
            final var joiner = new StringJoiner("|");
            joiner.add(use).add(type).add(system).add(value).add(period).add(assigner);
            return this.addMetadata(PropCsvValueSetExporter.MetadataColumns.RESOURCE, joiner.toString());
        }

        public PropCsvValueSetExporter.Builder setDate(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.DATE, value);
        }

        public PropCsvValueSetExporter.Builder setVersion(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.VERSION, value);
        }

        public PropCsvValueSetExporter.Builder setName(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.NAME, value);
        }

        public PropCsvValueSetExporter.Builder setTitle(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.TITLE, value);
        }

        public PropCsvValueSetExporter.Builder setStatus(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.STATUS, value);
        }

        public PropCsvValueSetExporter.Builder setDescription(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.DESCRIPTION, value);
        }

        public PropCsvValueSetExporter.Builder setCopyright(String value) {
            return this.setMetadata(PropCsvValueSetExporter.MetadataColumns.COPYRIGHT, value);
        }

        public PropCsvValueSetExporter build() throws Exception {
            // Safe values in temp variables
            var destSeperator = this.destSeperator;
            var destValueWrapper = this.destValueWrapper;
            var filters = this.filters;
            var staticTransformations = this.staticTransformations;
            var columnTransformations = this.columnTransformations;
            var metadataValues = this.metadataValues;
            // Clear previous values and reset to default
            this.reset();
            return new PropCsvValueSetExporter(destSeperator, destValueWrapper, filters, staticTransformations, columnTransformations, metadataValues);
        }

        public void reset() {
            this.destSeperator = PropCsvExporter.DEFAULT_DEST_SEPERATOR;
            this.destValueWrapper = PropCsvExporter.DEFAULT_DEST_VALUE_WRAPPER;
            this.filters = new HashMap<>();
            this.staticTransformations = new HashMap<>();
            this.columnTransformations = new HashMap<>();
            this.metadataValues = new HashMap<>();
        }

    }

    public enum MetadataColumns implements HasCardinality<ColumnCardinality> {

        RESOURCE("resource", ColumnCardinality.EXACTLY_ONCE), ID("id", ColumnCardinality.EXACTLY_ONCE),
        URL("url", ColumnCardinality.AT_MOST_ONCE), IDENTIFIER("identifier", ColumnCardinality.ANY),
        DATE("date", ColumnCardinality.AT_MOST_ONCE), VERSION("version", ColumnCardinality.AT_MOST_ONCE),
        NAME("name", ColumnCardinality.AT_MOST_ONCE), TITLE("title", ColumnCardinality.AT_MOST_ONCE),
        STATUS("status", ColumnCardinality.EXACTLY_ONCE),
        DESCRIPTION("description", ColumnCardinality.AT_MOST_ONCE),
        COPYRIGHT("copyright", ColumnCardinality.AT_MOST_ONCE);

        private final String rowName;
        private final ColumnCardinality cardinality;

        MetadataColumns(String rowName) {
            this.rowName = rowName;
            this.cardinality = ColumnCardinality.AT_MOST_ONCE;
        }

        MetadataColumns(String rowName, ColumnCardinality cardinality) {
            this.rowName = rowName;
            this.cardinality = cardinality;
        }

        @Override
        public String toString() {
            return this.rowName;
        }

        @Override
        public ColumnCardinality getCardinality() {
            return this.cardinality;
        }

        @Override
        public boolean inBounds(int number) {
            return false;
        }

        public boolean isRequired() {
            return this.cardinality.isRequired();
        }

    }

    public enum ConceptColumns implements HasCardinality<ColumnCardinality> {

        SYSTEM("system", ColumnCardinality.AT_MOST_ONCE),
        CODE("code", ColumnCardinality.EXACTLY_ONCE),
        DISPLAY("display", ColumnCardinality.AT_MOST_ONCE),
        DESIGNATION("designation", ColumnCardinality.ANY),
        FILTER("filter", ColumnCardinality.ANY),
        EXCLUDE("exclude", ColumnCardinality.AT_MOST_ONCE);

        private final String rowName;
        private final ColumnCardinality cardinality;

        ConceptColumns(String rowName) {
            this.rowName = rowName;
            this.cardinality = ColumnCardinality.AT_MOST_ONCE;
        }

        ConceptColumns(String rowName, ColumnCardinality cardinality) {
            this.rowName = rowName;
            this.cardinality = cardinality;
        }

        @Override
        public String toString() {
            return this.rowName;
        }

        @Override
        public ColumnCardinality getCardinality() {
            return this.cardinality;
        }

        @Override
        public boolean inBounds(int number) {
            return false;
        }

        public boolean isRequired() {
            return this.cardinality.isRequired();
        }

    }

}
