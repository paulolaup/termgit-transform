package ladr.termgit.transform;

import ladr.termgit.transform.csv.CsvConverter;
import ladr.termgit.transform.fhir.ValueSetCompositionRules;
import ladr.termgit.transform.propcsv.PropCsvCodeSystemExporter;

import java.nio.file.Path;

public class ConvertLoincParts {

    public static void main(String[] args) throws Exception {

        if (args.length < 3) throw new IllegalArgumentException("At least three arguments are required");

        final var src = Path.of(args[0]).toFile();
        final var csDest = Path.of(args[1]).toFile();
        final var vsDest = Path.of(args[2]).toFile();

        try{
            final var csvConverter = CsvConverter.builder()
                    .srcSeperator(',')
                    .srcValueWrapper('\"')
                    .addColumnTransformation("PartNumber", "PART_NUMBER", v -> v)
                    .addColumnTransformation("PartTypeName", "PART_TYPE_NAME", v -> v)
                    .addColumnTransformation("PartName", "PART_NAME", v -> v)
                    .addColumnTransformation("PartDisplayName", "PART_DISPLAY_NAME", v -> v)
                    .addColumnTransformation("Status", "STATUS", v -> v)
                    .build();

            final var propCsvCodeSystemExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc-parts")
                    .setUrl("http://fhir.ladr.de/terminology/loinc-parts")
                    .setName("LOINC Parts")
                    .setTitle("LOINC Parts")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "PART_NUMBER")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "PART_NAME")
                    .addColumnTransformation(v -> "en||"+v[0], PropCsvCodeSystemExporter.ConceptColumns.DESIGNATION, "PART_DISPLAY_NAME")
                    .addColumnTransformation(v -> "part-type-name|"+v[0].toLowerCase()+"|code", PropCsvCodeSystemExporter.ConceptColumns.PROPERTY, "PART_TYPE_NAME")
                    .build();

            final var data = csvConverter.convert(src);
            propCsvCodeSystemExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
        }
        catch (Exception exception) {
            throw new Exception("Converting LOINC Part file failed", exception);
        }

    }

}
