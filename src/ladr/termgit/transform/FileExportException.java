package ladr.termgit.transform;

public class FileExportException extends Exception {

	private static final long serialVersionUID = 8230976044667300694L;

	public FileExportException(String message) {
		super(message);
	}
	
	public FileExportException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
