package ladr.termgit.transform;

public class FileConversionException extends Exception {

    public FileConversionException(String message) {
        super(message);
    }

    public FileConversionException(String message, Exception cause) {
        super(message, cause);
    }

}
