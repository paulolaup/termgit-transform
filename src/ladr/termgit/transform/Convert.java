package ladr.termgit.transform;

import java.nio.file.Path;

import ladr.termgit.transform.csv.CsvConverter;
import ladr.termgit.transform.fhir.ValueSetCompositionRules;
import ladr.termgit.transform.propcsv.PropCsvCodeSystemExporter;

public class Convert {

    public static void main(String[] args) throws Exception {

        if (args.length < 3) throw new IllegalArgumentException("At least three arguments are required");

        final var src = Path.of(args[0]).toFile();
        final var csDest = Path.of(args[1]).toFile();
        final var vsDest = Path.of(args[2]).toFile();

        try{
            final var loincConverter = CsvConverter.builder()
                    .srcSeperator(',')
                    .srcValueWrapper('"')
                    .addColumnTransformation("LOINC_NUM", "CONCEPT_ID",  v -> v )
                    .addColumnTransformation("LONG_COMMON_NAME", "LONG_COMMON_NAME", v -> v)
                    .addColumnTransformation("COMPONENT", "COMPONENT", v -> v)
                    .addColumnTransformation("PROPERTY", "PROPERTY", v -> v)
                    .addColumnTransformation("TIME_ASPCT", "TIME_ASPECT", v -> v)
                    .addColumnTransformation("SYSTEM", "SYSTEM", v -> v)
                    .addColumnTransformation("SCALE_TYP", "SCALE", v -> v)
                    .addColumnTransformation("METHOD_TYP", "METHOD", v -> v)
                    .build();

            final var propCsvCodeSystemExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc")
                    .setUrl("http://loinc.org")
                    .setName("LOINC")
                    .setTitle("LOINC")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "CONCEPT_ID")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "LONG_COMMON_NAME")
                    .addColumnTransformation(v -> String.join(":", v), PropCsvCodeSystemExporter.ConceptColumns.DEFINITION,
                            "COMPONENT", "PROPERTY", "TIME_ASPECT", "SYSTEM", "SCALE", "METHOD")
                    .addColumnTransformation(v -> "component|"+v[0]+"|code", PropCsvCodeSystemExporter.ConceptColumns.PROPERTY, "COMPONENT")
                    .addColumnTransformation(v -> "property|"+v[0]+"|code", PropCsvCodeSystemExporter.ConceptColumns.PROPERTY, "PROPERTY")
                    .addColumnTransformation(v -> "time-aspect|"+v[0]+"|code", PropCsvCodeSystemExporter.ConceptColumns.PROPERTY, "TIME_ASPECT")
                    .addColumnTransformation(v -> "system|"+v[0]+"|code", PropCsvCodeSystemExporter.ConceptColumns.PROPERTY, "SYSTEM")
                    .addColumnTransformation(v -> "scale|"+v[0]+"|code", PropCsvCodeSystemExporter.ConceptColumns.PROPERTY, "SCALE")
                    .addColumnTransformation(v -> v[0].length() > 0 ? "method|"+v[0]+"|code" : "", PropCsvCodeSystemExporter.ConceptColumns.PROPERTY, "METHOD")
                    .build();

            final var data = loincConverter.convert(src);
            propCsvCodeSystemExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
        }
        catch (Exception exception) {
            throw new Exception("Failed to build or run LoincConverter instance", exception);
        }

    }

}
