package ladr.termgit.transform;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class FileExporter {

    protected static BufferedWriter writeToFile(File dest) throws IOException {
        final var destPath = dest.getAbsolutePath();
        try {
            return new BufferedWriter(new FileWriter(destPath));
        }
        catch (IOException ioException) {
            throw new IOException("Could not open/create destination file @ "+destPath, ioException);
        }
    }

    protected static void writeLine(String line, BufferedWriter writer) throws IOException {
        writer.write(line);
        writer.newLine();
    }

    /**
     * Returns the file extension of the output file
     *
     * @return String with pattern '\.[.]*' (extension has to start with '.' character)
     */
    public abstract String getExtension();

}
