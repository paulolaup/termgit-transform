package ladr.termgit.transform;

import java.nio.file.Path;

import ladr.termgit.transform.csv.CsvConverter;
import ladr.termgit.transform.fhir.ValueSetCompositionRules;
import ladr.termgit.transform.propcsv.PropCsvCodeSystemExporter;

public class CreateLoincPartTerminologyData {

	public static void main(String[] args) throws Exception {

        try{
        	var src = Path.of("input", "Part.csv").toFile();
            var csDest = Path.of("output", "CodeSystem-loinc-component.1.propcsv.csv").toFile();
            var vsDest = Path.of("output", "ValueSet-loinc-component.1.propcsv.csv").toFile();
        	
            final var loincConverter = CsvConverter.builder()
                    .srcSeperator(',')
                    .srcValueWrapper('"')
                    .addColumnTransformation("PartNumber", "PART_NUMBER",  v -> v )
                    .addColumnTransformation("PartTypeName", "PART_TYPE_NAME", v -> v)
                    .addColumnTransformation("PartName", "PART_NAME",  v -> v )
                    .addColumnTransformation("PartDisplayName", "PART_DISPLAY_NAME",  v -> v )
                    .addColumnTransformation("Status", "STATUS",  v -> v )
                    .build();

            final var loincComponentExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc")
                    .setUrl("http://fhir.ladr.de/terminology/loinc-component")
                    .setName("LOINC Component")
                    .setTitle("LOINC Component Code System")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addFilter(v -> v[0].equals("COMPONENT"), "PART_TYPE_NAME")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "PART_NUMBER")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "PART_NAME")
                    .addColumnTransformation(v -> "en|http://snomed.info/sct|900000000000003001||"+v[0], PropCsvCodeSystemExporter.ConceptColumns.DESIGNATION, "PART_DISPLAY_NAME")
                    .build();

            final var data = loincConverter.convert(src);
            loincComponentExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
            
            csDest = Path.of("output", "CodeSystem-loinc-property.1.propcsv.csv").toFile();
            vsDest = Path.of("output", "ValueSet-loinc-propery.1.propcsv.csv").toFile();
            
            final var loincPropertyExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc")
                    .setUrl("http://fhir.ladr.de/terminology/loinc-property")
                    .setName("LOINC Property")
                    .setTitle("LOINC Property Code System")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addFilter(v -> v[0].equals("PROPERTY"), "PART_TYPE_NAME")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "PART_NUMBER")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "PART_NAME")
                    .addColumnTransformation(v -> "en|http://snomed.info/sct|900000000000003001||"+v[0], PropCsvCodeSystemExporter.ConceptColumns.DESIGNATION, "PART_DISPLAY_NAME")
                    .build();

            loincPropertyExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
            
            csDest = Path.of("output", "CodeSystem-loinc-time-aspect.1.propcsv.csv").toFile();
            vsDest = Path.of("output", "ValueSet-loinc-time-aspect.1.propcsv.csv").toFile();
            
            final var loincTimeAspectExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc")
                    .setUrl("http://fhir.ladr.de/terminology/loinc-time-aspect")
                    .setName("LOINC Time Aspect")
                    .setTitle("LOINC Time Aspect Code System")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addFilter(v -> v[0].equals("TIME"), "PART_TYPE_NAME")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "PART_NUMBER")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "PART_NAME")
                    .addColumnTransformation(v -> "en|http://snomed.info/sct|900000000000003001||"+v[0], PropCsvCodeSystemExporter.ConceptColumns.DESIGNATION, "PART_DISPLAY_NAME")
                    .build();

            loincTimeAspectExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
            
            csDest = Path.of("output", "CodeSystem-loinc-system.1.propcsv.csv").toFile();
            vsDest = Path.of("output", "ValueSet-loinc-system.1.propcsv.csv").toFile();
            
            final var loincSystemExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc")
                    .setUrl("http://fhir.ladr.de/terminology/loinc-system")
                    .setName("LOINC System")
                    .setTitle("LOINC System Code System")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addFilter(v -> v[0].equals("SYSTEM"), "PART_TYPE_NAME")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "PART_NUMBER")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "PART_NAME")
                    .addColumnTransformation(v -> "en|http://snomed.info/sct|900000000000003001||"+v[0], PropCsvCodeSystemExporter.ConceptColumns.DESIGNATION, "PART_DISPLAY_NAME")
                    .build();

            loincSystemExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
            
            csDest = Path.of("output", "CodeSystem-loinc-scale.1.propcsv.csv").toFile();
            vsDest = Path.of("output", "ValueSet-loinc-scale.1.propcsv.csv").toFile();
            
            final var loincScaleExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc")
                    .setUrl("http://fhir.ladr.de/terminology/loinc-scale")
                    .setName("LOINC Scale")
                    .setTitle("LOINC Scale Code System")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addFilter(v -> v[0].equals("SCALE"), "PART_TYPE_NAME")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "PART_NUMBER")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "PART_NAME")
                    .addColumnTransformation(v -> "en|http://snomed.info/sct|900000000000003001||"+v[0], PropCsvCodeSystemExporter.ConceptColumns.DESIGNATION, "PART_DISPLAY_NAME")
                    .build();

            loincScaleExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
            
            csDest = Path.of("output", "CodeSystem-loinc-method.1.propcsv.csv").toFile();
            vsDest = Path.of("output", "ValueSet-loinc-method.1.propcsv.csv").toFile();
            
            final var loincMethodExporter = PropCsvCodeSystemExporter.builder()
                    .destSeperator(';')
                    .destValueWrapper('"')
                    .setId("loinc")
                    .setUrl("http://fhir.ladr.de/terminology/loinc-method")
                    .setName("LOINC Method")
                    .setTitle("LOINC Method Code System")
                    .setStatus("active")
                    .setContent("complete")
                    .setVersion("2.7.4")
                    .addFilter(v -> v[0].equals("METHOD"), "PART_TYPE_NAME")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.CODE, "PART_NUMBER")
                    .addColumnTransformation(v -> v[0], PropCsvCodeSystemExporter.ConceptColumns.DISPLAY, "PART_NAME")
                    .addColumnTransformation(v -> "en|http://snomed.info/sct|900000000000003001||"+v[0], PropCsvCodeSystemExporter.ConceptColumns.DESIGNATION, "PART_DISPLAY_NAME")
                    .build();

            loincMethodExporter.exportWithAssociatedValueSet(data, csDest, vsDest, ValueSetCompositionRules.EXPLICIT);
        }
        catch (Exception exception) {
            throw new Exception("Failed to build or run LoincConverter instance", exception);
        }

	}

}
