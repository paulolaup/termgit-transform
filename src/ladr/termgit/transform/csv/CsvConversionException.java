package ladr.termgit.transform.csv;

import ladr.termgit.transform.FileConversionException;

public class CsvConversionException extends FileConversionException {

    public CsvConversionException(String message) { super(message); }

    public CsvConversionException(String message, Exception cause) {
        super(message, cause);
    }

}
