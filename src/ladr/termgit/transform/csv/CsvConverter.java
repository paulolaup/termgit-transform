package ladr.termgit.transform.csv;

import ladr.termgit.transform.*;
import ladr.termgit.transform.datamodel.ManagedTable;
import ladr.termgit.transform.datamodel.Table;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CsvConverter extends FileConverter<Table> {

    private final char srcSeperator;
    private final char srcValueWrapper;
    private final HashMap<String[], Predicate<String[]>> filters;
    private final HashMap<String, Function<String, String>> columnTransformations;
    private final HashMap<String, String> columnMapping;

    public CsvConverter(char srcSeperator, char srcValueWrapper,
    					HashMap<String[], Predicate<String[]>> filters,
                        HashMap<String, Function<String, String>> columnTransformations,
                        HashMap<String, String> columnMapping) {
        this.srcSeperator = srcSeperator;
        this.srcValueWrapper = srcValueWrapper;
        this.filters = filters;
        this.columnTransformations = columnTransformations;
        this.columnMapping = columnMapping;
    }

    @Override
    public Table convert(File src) throws IOException, FileConversionException {
        System.out.println("Converting file @ "+src.getAbsolutePath());
        Table table;

        try (var reader = FileConverter.readFromFile(src)) {
            // Read first line containing column names
            var firstLine = reader.readLine();
            if (firstLine == null) throw new CsvConversionException("Source file is empty");
            final var srcColumnNames = splitLine(firstLine);
            // Create column index for source file to get associated value of row by column name
            final var srcColumnIdx = IntStream.range(0, srcColumnNames.length).boxed()
                    .collect(Collectors.toMap(i -> srcColumnNames[i], i -> i));

            // Write column names to destination file
            final var destColumnNames = new ArrayList<String>(this.columnMapping.keySet()).toArray(new String[0]);
            table = new ManagedTable(destColumnNames);

            // Read consequent lines containing values and write transformed values to destination file
            // Unfortunate use of loop due to inability to throw uncaught exceptions in lambdas
            var lineCnt = 1;
            for (var line = reader.readLine(); line != null; line = reader.readLine()) {
                System.out.print("Line " + lineCnt + "\r");
                final var columnValues = splitLine(line);
                final var entry = new String[destColumnNames.length];

                if (columnValues.length != srcColumnNames.length) {
                    System.out.println(line);
                    throw new CsvConversionException("Unequal columns and values count: "
                            + srcColumnNames.length + " vs " + columnValues.length +
                            "\ncolumns: [" + String.join("|", srcColumnNames) +
                            "\nvalues: [" + String.join("|", columnValues) + "]");
                }
                
                final var passesFilters = this.filters.entrySet().stream().allMatch(filterEntry -> {
            		final var srcColumnValues = (String[]) Arrays.stream(filterEntry.getKey()).map(c -> columnValues[srcColumnIdx.get(c)]).toArray(String[]::new);
                	final var filter = filterEntry.getValue();
                	return filter.test(srcColumnValues);
            	});
                
                //Skip line processing if it doesn't pass filter conditions
                if (passesFilters) {
                	for (int i = 0; i < destColumnNames.length; i++) {
                        final var destColumnName = destColumnNames[i];
                        final var srcColumnName = this.columnMapping.get(destColumnName);
                        final var transformation = columnTransformations.get(destColumnName);
                        final var srcColumnValue = columnValues[srcColumnIdx.get(srcColumnName)];

                        entry[i] = transformation.apply(srcColumnValue);
                    }

                    table.addEntry(entry);
                }

                lineCnt++;
            }
            System.out.println("Saved results in table");
        } catch (TableException exception) {
            throw new CsvConversionException("Failed to add transformed line to results", exception);
        }

        return table;
    }

    protected String[] splitLine(String line) {
        final char srcValueWrapper = this.getSrcValueWrapper(), srcSeperator = this.getSrcSeperator();
        final var list = new ArrayList<String>();
        final var chars = (line + srcSeperator).toCharArray();
        int wrapperCharCnt = 0, startIdx = 1;
        for (int i = 0; i < chars.length; i++) {
            final var c = chars[i];
            if (c == srcValueWrapper) wrapperCharCnt++;
            else if (c == srcSeperator && wrapperCharCnt % 2 == 0) {
                list.add(new String(Arrays.copyOfRange(chars, startIdx, i - 1)));
                wrapperCharCnt = 0;
                startIdx = i + 2;
            }
        }
        var result = new String[list.size()];
        return list.toArray(result);
    }

    public char getSrcSeperator() {
        return srcSeperator;
    }

    public char getSrcValueWrapper() {
        return srcValueWrapper;
    }
    
    public HashMap<String[], Predicate<String[]>> getFilters() {
    	return filters;
    }

    public HashMap<String, Function<String, String>> getColumnTransformations() {
        return columnTransformations;
    }

    public HashMap<String, String> getColumnMapping() {
        return columnMapping;
    }

    public static CsvConverter.Builder builder() {
        return new CsvConverter.Builder();
    }

    public static class Builder implements IBuilder<CsvConverter> {

        public static final char SEPERATOR_DEFAULT = ',';
        public static final char VALUE_WRAPPER_DEFAULT = '"';

        private char srcSeperator = SEPERATOR_DEFAULT;
        private char srcValueWrapper = VALUE_WRAPPER_DEFAULT;
        private HashMap<String[], Predicate<String[]>> filters = new HashMap<>();
        private HashMap<String, Function<String, String>> columnTransformations = new HashMap<>();
        private HashMap<String, String> columnMapping = new HashMap<>();

        public Builder srcSeperator(char seperator) {
            this.srcSeperator = seperator;
            return this;
        }

        public Builder srcValueWrapper(char valueWrapper) {
            this.srcValueWrapper = valueWrapper;
            return this;
        }
        
        public Builder addFilter(Predicate<String[]> filter, String... srcColumns) throws Exception {
        	if (this.filters.containsKey(srcColumns))
        		throw new Exception("Filter for source columns '"+srcColumns.toString()+"' already defined");
        	this.filters.put(srcColumns, filter);
        	return this;
        }

        public Builder addColumnTransformation(String srcColumn, String destColumn, Function<String, String> transformation) throws Exception {
            if (this.columnMapping.containsKey(destColumn))
                throw new Exception("Transformation for destination column + '"+destColumn+"' already present");
            this.columnTransformations.put(destColumn, transformation);
            this.columnMapping.put(destColumn, srcColumn);
            return this;
        }

        public CsvConverter build() {
            // Safe values in temp variables
            var srcSeperator = this.srcSeperator;
            var srcValueWrapper = this.srcValueWrapper;
            var filters = this.filters;
            var columnTransformations = this.columnTransformations;
            var columnMapping = this.columnMapping;
            // Clear previous values and reset to default
            this.reset();
            return new CsvConverter(srcSeperator, srcValueWrapper, filters, columnTransformations, columnMapping);
        }

        public void reset() {
            this.srcSeperator = SEPERATOR_DEFAULT;
            this.srcValueWrapper = VALUE_WRAPPER_DEFAULT;
            this.filters = new HashMap<>();
            this.columnTransformations = new HashMap<>();
            this.columnMapping = new HashMap<>();
        }

        public char getSrcSeperator() {
            return srcSeperator;
        }

        public char getSrcValueWrapper() {
            return srcValueWrapper;
        }
        
        public HashMap<String[], Predicate<String[]>> getFilters() {
        	return filters;
        }

        public HashMap<String, Function<String, String>> getColumnTransformations() {
            return columnTransformations;
        }

        public HashMap<String, String> getColumnMapping() {
            return columnMapping;
        }

    }

}