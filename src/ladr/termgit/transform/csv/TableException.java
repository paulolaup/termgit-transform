package ladr.termgit.transform.csv;

public class TableException extends Exception {

    public TableException(String message) { super(message); }

    public TableException(String message, Throwable cause) { super(message, cause); }

}
