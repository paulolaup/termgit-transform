package ladr.termgit.transform.datamodel;

public class Tuple <T, U> {

    private final T first;
    private final U second;

    public Tuple(T first, U second) {
        this.first = first;
        this.second = second;
    }

    public T first() {
        return first;
    }

    public U second() {
        return second;
    }

}
