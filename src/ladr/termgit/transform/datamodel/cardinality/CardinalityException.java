package ladr.termgit.transform.datamodel.cardinality;

public class CardinalityException extends Exception {

    public CardinalityException(String message) {
        super(message);
    }

    public CardinalityException(String message, Throwable cause) {
        super(message, cause);
    }

}
