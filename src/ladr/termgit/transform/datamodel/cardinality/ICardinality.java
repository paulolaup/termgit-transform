package ladr.termgit.transform.datamodel.cardinality;

import java.util.Arrays;

public interface ICardinality {

    int lower();

    int upper();

    int[] bounds();

    boolean inBounds(int number);

    static boolean inBounds(int number, ICardinality cardinality) {
        return cardinality.inBounds(number);
    }

    @SuppressWarnings("unchecked")
    static <T extends Enum<T> & HasCardinality<?>> T[] getRequired(Class<T> clazz) {
        return (T[]) Arrays.stream(clazz.getEnumConstants()).filter(e -> e.isRequired()).toArray();
    }

}
