package ladr.termgit.transform.datamodel.cardinality;

public interface HasCardinality<T extends ICardinality> {

    T getCardinality();

    boolean inBounds(int number);

    boolean isRequired();

}
