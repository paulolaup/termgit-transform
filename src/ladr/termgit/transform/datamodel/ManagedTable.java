package ladr.termgit.transform.datamodel;

import ladr.termgit.transform.csv.TableException;
import java.util.TreeSet;

public class ManagedTable extends Table {

    private final Manager manager;

    public ManagedTable(String... columnNames) {
        super(columnNames);
        this.manager = new Manager();
    }

    @Override
    public int addEntry(String... values) throws TableException {
        final int rowNumber = this.manager.getFreeIdAndAssign();
        super.addEntry(rowNumber, values);
        return rowNumber;
    }

    @Override
    public String[] deleteEntry(int rowNumber) throws TableException {
        this.manager.freeAssignedId(rowNumber);
        return super.deleteEntry(rowNumber);
    }

    public class Manager {

        private int idRangeMax = 0;
        //TODO: Try this with range set (should only be a worth while alternative while using a lot of IDs)
        private final TreeSet<Integer> freeIds = new TreeSet<>();

        public Manager() {
            this.freeIds.add(0);
        }

        public synchronized int getFreeIdAndAssign() {
            if(freeIds.isEmpty()){
                idRangeMax++;
                return idRangeMax;
            }
            final var id = freeIds.pollFirst();
            freeIds.remove(id);
            // Should never be empty due to prior check
            return id;
        }

        public synchronized void freeAssignedId(int id) throws TableException {
            if(id < 0) throw new TableException("Negative integers are not valid IDs");
            if(id >= idRangeMax) throw new TableException("ID "+id+" was above current max ID value "+this.idRangeMax);
            this.freeIds.add(id);
        }

    }

}
