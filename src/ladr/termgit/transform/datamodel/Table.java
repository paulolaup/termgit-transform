package ladr.termgit.transform.datamodel;

import ladr.termgit.transform.csv.TableException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Table implements Iterable<String[]> {

    private final String[] columnNames;
    private final ArrayList<String[]> entries;

    public Table(String... columnNames) {
        this.columnNames = columnNames;
        this.entries = new ArrayList<>();
    }

    public String[] getEntry(int rowNumber) throws TableException {
        try {
            return this.entries.get(rowNumber);
        }
        catch (Exception exception) {
            throw new TableException("Failed to access table entry", exception);
        }
    }

    public String getValue(int rowNumber, int colNumber) throws TableException {
        try {
            return this.entries.get(rowNumber)[colNumber];
        }
        catch (Exception exception) {
            throw new TableException("Failed to access table value", exception);
        }
    }

    public void addEntry(int rowNumber, String... values) throws TableException {
        if (values.length != this.columnNames.length) throw new TableException("Mismatch between number of values and "+
                "columns: "+values.length+" values provided vs "+this.columnNames.length+" columns available");
        this.entries.add(rowNumber, values);
    }

    public int addEntry(String... values) throws TableException {
        if (values.length != this.columnNames.length) throw new TableException("Mismatch between number of values and "+
                "columns: "+values.length+" values provided vs "+this.columnNames.length+" columns available");
        this.entries.add(values);
        return this.entries.size();
    }

    public void editValue(int rowNumber, int columnNumber, String value) throws TableException {
        try {
            this.entries.get(rowNumber)[columnNumber] = value;
        }
        catch (IndexOutOfBoundsException exception) {
            throw new TableException("Coordinates for value exceed table dimensions", exception);
        }
    }

    public String[] deleteEntry(int rowNumber) throws TableException {
        try {
            return this.entries.remove(rowNumber);
        }
        catch (IndexOutOfBoundsException exception) {
            throw new TableException("Row number exceeded table row count", exception);
        }
    }

    @Override
    public Iterator<String[]> iterator() {
        return this.entries.iterator();
    }

    public Iterator<String[]> iterator(int start, int end) {
        return this.entries.subList(start, end).iterator();
    }

    @Override
    public void forEach(Consumer<? super String[]> action) {
        this.entries.forEach(action);
    }

    @Override
    public Spliterator<String[]> spliterator() {
        return this.entries.spliterator();
    }

    protected void ensureCapacity(int size) {
        this.entries.ensureCapacity(size);
    }

    public String[] getColumnNames() {
        return columnNames;
    }

}
