package ladr.termgit.transform;

import java.io.File;
import java.io.IOException;

public interface IFileConverter<T> {

    T convert(File src) throws IOException, FileConversionException;

}
