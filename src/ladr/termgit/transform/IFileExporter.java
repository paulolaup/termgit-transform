package ladr.termgit.transform;

import java.io.File;
import java.io.IOException;

public interface IFileExporter<T> {

    void export(T data, File dest) throws IOException, FileExportException;

}
